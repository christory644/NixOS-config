{ config, pkgs, inputs, username, ... }:
let
  inherit (import ./options.nix)
    username
    gitUsername
    gitEmail
    flakeDir;
in {
  # TODO please change the username & home direcotry to your own
  home.username = "${username}";
  home.homeDirectory = "/home/${username}";
  home.stateVersion = "23.11";

  imports = [
    ./config/home
  ];

  # Packages that should be installed to the user profile.
  home.packages = with pkgs; [
  ];

  # basic configuration of git, please change to your own
  programs.git = {
    enable = true;
    userName = "${gitUsername}";
    userEmail = "${gitEmail}";
  };

  # Let home Manager install and manage itself.
  programs.home-manager.enable = true;
}
