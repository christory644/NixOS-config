{
  description = "NixOS Flake for my systems";

  inputs = {
    # NixOS official package source
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    home-manager.url = "github:nix-community/home-manager/release-23.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ self, nixpkgs, nixos-hardware, home-manager, ... }:
    let
      system = "x86_64-linux";
      inherit (import ./options.nix) username hostname;
      pkgs = import nixpkgs {
        inherit system;
	config = {
	  allowUnfree = true;
	};
      };
    in {
      nixosConfigurations = {
        "${hostname}" = nixpkgs.lib.nixosSystem {
	  specialArgs = {
	    inherit system;
	    inherit inputs;
	    inherit username;
	    inherit hostname;
	  };
        modules = [
          nixos-hardware.nixosModules.microsoft-surface-common
          # import previous configuration.nix in order to convert to flakes with minimal disruption
	  ./configuration.nix
	  ./system.nix
	  home-manager.nixosModules.home-manager
	  {
	    home-manager.extraSpecialArgs = {
	      inherit username;
	      inherit inputs;
            };
	    home-manager.useGlobalPkgs = true;
	    home-manager.useUserPackages = true;
	    home-manager.backupFileExtension = "backup";
	    home-manager.users.christory = import ./home.nix;
	  }
        ];
      };
    };
  };
}
