{ pkgs, config, inputs, ... }:

{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # System wide applications
  environment.systemPackages = with pkgs; [
    bat
    btop
    cht-sh
    cmatrix
    cowsay
    curl
    git
    htop
    lsd
    lshw
    mdcat
    neofetch
    nh
    ripgrep
    unrar
    unzip
    vim
    wget
    wl-clipboard
  ];

  # System wide programs
  programs = {
    # fixes gtk theme elements in wayland, such as minimize/maximize window elements
    dconf.enable = true;

    # network diagnostics tooling, like ping and traceroute combined
    mtr.enable = true;
  };
}
