{ pkgs, config, lib, includeSurfaceControls, ... }:

let inherit (import ../../options.nix) includeSurfaceControls; in
lib.mkIf includeSurfaceControls {
  # Microsoft surface controls
  microsoft-surface = {
    ipts.enable = true;
    surface-control.enable = true;
  };

  environment.systemPackages = with pkgs; [
    linux-firmware
    libwacom-surface
  ];
}

