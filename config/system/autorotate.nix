{ pkgs, config, lib, ... }:

let inherit (import ../../options.nix) enableAutoration; in
lib.mkIf (enableAutoration == true) {
  # Enable autorotation
  hardware.sensor.iio.enable = true;
}
