{ config, pkgs, ... }:

{
  imports = [
    ./autorotate.nix
    ./boot.nix
    ./microsoft-surface.nix
    ./nvidia.nix
    ./packages.nix
  ];
}
