# NixOS System Configuration 
This repository contains my NixOS system configurations. It is not meant to be a turnkey solution to copying my setup or learning Nix. I've tried to use simple Nix practices wherever possible, but if you wish to copy this from you, you'll have to learn at least the basics of Nix, NixOS, etc.

I don't claim to be an expert at Nix or NixOS, so there are certainly improvements that could be made. Feel free to suggest them, but please don't be offended if I don't integrate them. I value having my configs work in a way I fully understand over having it be optimal.
