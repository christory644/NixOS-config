{ inputs, config, pkgs, username, hostname, ... }:

let
  inherit (import ./options.nix)
    flakeDir
    gitUsername
    userKBDLayout
    userLocale
    userLocaleExtraVariable
    userShell
    systemTimeZone;
in {
  imports = [
    ./hardware.nix
    ./config/system
  ];

  # Enable networking
  networking.hostName = "${hostname}";
  networking.networkmanager.enable = true;

  # Set time zone
  time.timeZone = "${systemTimeZone}";

  # Set internationalization properties
  i18n.defaultLocale = "${userLocale}";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "${userLocaleExtraVariable}";
    LC_IDENTIFICATION = "${userLocaleExtraVariable}";
    LC_MEASUREMENT = "${userLocaleExtraVariable}";
    LC_MONETARY = "${userLocaleExtraVariable}";
    LC_NAME = "${userLocaleExtraVariable}";
    LC_NUMERIC = "${userLocaleExtraVariable}";
    LC_PAPER = "${userLocaleExtraVariable}";
    LC_TELEPHONE = "${userLocaleExtraVariable}";
    LC_TIME = "${userLocaleExtraVariable}";
  };

  # Set keyboard layout
  console.keyMap = "${userKBDLayout}";

  # Define user account(s)
  users = {
    mutableUsers = true;
    users."${username}" = {
      homeMode = "755";
      hashedPassword = "$6$3mkdVXyUprFRBYxy$AVUyrdQr6JJmQZuiRI.4UtkptJCn34Mb10i5YvebA6MhWXfH01VBFuCbwrUvvcQ9HUXas4nF22K/s/ZNPMS4T.$6$3mkdVXyUprFRBYxy$AVUyrdQr6JJmQZuiRI.4UtkptJCn34Mb10i5YvebA6MhWXfH01VBFuCbwrUvvcQ9HUXas4nF22K/s/ZNPMS4T.";
      isNormalUser = true;
      description = "${gitUsername}";
      extraGroups = [ "networkmanager" "wheel" "libvirtd" "docker" ];
      shell = pkgs.${userShell};
      ignoreShellProgramCheck = true;
      packages = with pkgs; [
	kitty
	neovim
	firefox
	discord
	vlc
	youtube-music
	spotify
	obsidian
      ];
    };
  };

  # Set system wide environment variables
  environment.variables = {
    FLAKE = "${flakeDir}";
    EDITOR = "vim";
  };

  # Configure nix optimization settings and garbage collection
  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };

    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };

  system.stateVersion = "23.11";
}
