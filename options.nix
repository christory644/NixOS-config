# set variables for various options that will be used elsewhere in the system config files

let
  username = "christory";
  hostname = "christorySLS";
  userHomeDir = "/home/${username}";
  userShell = "zsh"; # Possible options: bash, zsh
  flakeDir = "${userHomeDir}/NixOS-config"; # this should be the location that you cloned the repo to
  gitUsername = "Christopher Story";
  gitEmail = "christory@pm.me";
  timezone = "America/Chicago";
  isSurfaceDevice = true;
  isTabletDevice = true;
in {
  username = "${username}";
  hostname = "${hostname}";
  gitUsername = "${gitUsername}";
  gitEmail = "${gitEmail}";
  flakeDir = "${flakeDir}";
  flakePrev = "${userHomeDir}/.christoryOS/previous";
  flakeBackup = "${userHomeDir}/.christoryOS/backup";
  userKBDLayout = "us";
  userLocale = "en_US.UTF-8";
  userLocaleExtraVariable = "en_US.UTF-8";
  userShell = "${userShell}";
  systemTimeZone = "${timezone}";
  gpuType = "nvidia";
  includeSurfaceControls = isSurfaceDevice;
  enableAutoration = isTabletDevice;
}
